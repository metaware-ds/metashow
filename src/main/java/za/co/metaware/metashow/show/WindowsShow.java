package za.co.metaware.metashow.show;

import za.co.metaware.metashow.exception.TechnicalException;
import za.co.metaware.metashow.util.Configuration;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;

/**
 * Created by Sfa on 3/4/2017.
 */
public class WindowsShow implements Show {
    private final MenuListener menuListener;

    private TrayIcon icon;
    private MenuItem startMenuItem;
    private MenuItem stopMenuItem;
    private MenuItem settingMenuItem;
    private MenuItem exitMenuItem;

    private GraphicsDevice srcScreen;
    private GraphicsDevice trgScreen;

    private Projector projector;
    private Snap snapTool;

    private boolean showing;

    public WindowsShow() {
        menuListener = new MenuListener();
    }

    public boolean canShow() {
        return SystemTray.isSupported();
    }

    @Override
    public boolean isShowing() {
        return showing;
    }

    public void setShowing(boolean showing) {
        this.showing = showing;
        startMenuItem.setEnabled(!isShowing());
        stopMenuItem.setEnabled(isShowing());
    }

    @Override
    public void prepare() {
        Configuration configuration = Configuration.instance();
        if (configuration.getProperty(Configuration.MOUSE_COLOR) == null) {
            configuration.setProperty(Configuration.MOUSE_COLOR, Color.RED);
        }

        GraphicsDevice trgScreen = (GraphicsDevice) configuration.getProperty(Configuration.TARGET_SCREEN);
        Rectangle targetArea;
        if (trgScreen == null) {
            targetArea = new Rectangle(500, 300);
        } else {
            targetArea = trgScreen.getDefaultConfiguration().getBounds();
        }
        configuration.setProperty(Configuration.CAPTURE_AREA, targetArea);


        Toolkit toolkit = Toolkit.getDefaultToolkit();
        URL iconURL = WindowsShow.class.getResource("/icon.jpg");
        Image image = toolkit.getImage(iconURL);

        PopupMenu menu = preparePopupMenu();

        icon = new TrayIcon(image, "Metashow Presentation Software", menu);
        SystemTray tray = SystemTray.getSystemTray();
        try {
            tray.add(icon);
        } catch (AWTException e) {
            throw new RuntimeException("Failed to load tray", e);
        }
        setShowing(false);
    }

    @Override
    public void destroy() {
        SystemTray.getSystemTray().remove(icon);
    }

    @Override
    public void beginShow() {
        Configuration configuration = Configuration.instance();

        GraphicsEnvironment environment = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice[] screens = environment.getScreenDevices();
        if (screens.length <= 1) {
            throw new TechnicalException("No second screen detected.");
        }
        srcScreen = screens[0];
        trgScreen = screens[1];
        configuration.setProperty(Configuration.TARGET_SCREEN, trgScreen);
        if (!trgScreen.isFullScreenSupported()) {
            throw new TechnicalException("Full screen not support on this screen " + trgScreen.getIDstring());
        }
        projector = new Projector(trgScreen);
        trgScreen.setFullScreenWindow(projector);
        snapTool = new Snap(srcScreen, projector, 200);
        snapTool.config();
        snapTool.start();

        setShowing(true);
    }

    @Override
    public void endShow() {
        if (snapTool != null) {
            snapTool.stopRunning();
        }
        if (projector != null && projector.isVisible()) {
            trgScreen.setFullScreenWindow(projector);
            projector.dispose();
        }
        setShowing(false);
    }

    private PopupMenu preparePopupMenu() {
        PopupMenu menu = new PopupMenu("MetaShow");

        startMenuItem = new MenuItem("Start Presentation");
        startMenuItem.addActionListener(menuListener);

        stopMenuItem = new MenuItem("Stop Presentation");
        stopMenuItem.addActionListener(menuListener);

        settingMenuItem = new MenuItem("Settings");
        settingMenuItem.addActionListener(menuListener);

        exitMenuItem = new MenuItem("Exit");
        exitMenuItem.addActionListener(menuListener);

        menu.add(startMenuItem);
        menu.add(stopMenuItem);
        menu.addSeparator();
        menu.add(settingMenuItem);
        menu.addSeparator();
        menu.add(exitMenuItem);

        return menu;
    }

    private class MenuListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            Object source = e.getSource();
            if (source == startMenuItem) {
                try {
                    beginShow();
                } catch (TechnicalException ex) {
                    JOptionPane.showMessageDialog(null, ex.getMessage(), "Technical Error", JOptionPane.ERROR_MESSAGE);
                }
            } else if (source == stopMenuItem) {
                endShow();
            } else if (source == settingMenuItem) {
                Settings settings = new Settings();
                settings.setVisible(true);
            } else if (source == exitMenuItem) {
                endShow();
                destroy();
            }
        }
    }
}