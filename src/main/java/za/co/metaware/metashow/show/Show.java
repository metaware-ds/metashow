package za.co.metaware.metashow.show;

/**
 * Created by Sfa on 3/4/2017.
 */
public interface Show {
    boolean canShow();

    boolean isShowing();

    void prepare();

    void destroy();

    void beginShow();

    void endShow();
}
