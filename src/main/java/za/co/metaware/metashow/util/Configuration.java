package za.co.metaware.metashow.util;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by Sfa on 3/5/2017.
 */
public class Configuration {

    public static final String TARGET_SCREEN = "target_screen";
    public static final String CAPTURE_AREA = "capture_area";
    public static final String MOUSE_COLOR = "mouse_color";

    private static final Configuration instance = new Configuration();

    private Map<String, Object> properties;
    private Set<UpdateListener> updateListeners;

    private Configuration() {
        properties = new HashMap<>();
        updateListeners = new HashSet<>();
    }

    public static final Configuration instance() {
        return instance;
    }

    public Object setProperty(String key, Object value) {
        Object old = properties.put(key, value);
        notifyListeners(key, old, value);
        return old;
    }

    public Object getProperty(String key) {
        return properties.get(key);
    }

    public synchronized void addListener(UpdateListener listener) {
        updateListeners.add(listener);
    }

    public synchronized void removeListener(UpdateListener listener) {
        updateListeners.remove(listener);
    }

    protected synchronized void notifyListeners(String key, Object oldValue, Object newValue) {
        updateListeners.parallelStream().forEach(listener -> listener.update(key, oldValue, newValue));
    }

    public interface UpdateListener {
        void update(String key, Object oldValue, Object newValue);
    }
}
