package za.co.metaware.metashow;

import za.co.metaware.metashow.show.LinuxShow;
import za.co.metaware.metashow.show.Show;
import za.co.metaware.metashow.show.WindowsShow;

/**
 * Created by Sfa on 3/4/2017.
 */
public class MetaShow {
    public static void main(String[] args) throws InterruptedException {
        Show show = new WindowsShow();
        if (show.canShow()) {
            show.prepare();
        } else {
            show = new LinuxShow();
            show.prepare();
        }
    }
}
