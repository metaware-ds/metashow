package za.co.metaware.metashow.show;

/**
 * Created by Sfa on 3/4/2017.
 */
public class LinuxShow implements Show {
    private boolean showing;

    @Override
    public boolean canShow() {
        return true;
    }

    @Override
    public boolean isShowing() {
        return showing;
    }

    public void setShowing(boolean showing) {
        this.showing = showing;
    }

    @Override
    public void prepare() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void beginShow() {

    }

    @Override
    public void endShow() {

    }
}
