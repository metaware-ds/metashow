package za.co.metaware.metashow.show;

import za.co.metaware.metashow.util.Configuration;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Sfa on 3/5/2017.
 */
public class Settings extends JFrame {
    private final ButtonListener expert;

    private JLabel zoomLbl;
    private JButton zoomIn;
    private JButton zoomOut;

    private JButton colorButton;
    private Color mouseColor;

    public Settings() {
        super("MetaShow");

        expert = new ButtonListener();

        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setResizable(false);

        JPanel controls = createControlsPanel();

        getContentPane().add(controls);
        pack();
    }

    private JPanel createControlsPanel() {
        JPanel panel = new JPanel();

        Configuration configuration = Configuration.instance();
        Rectangle captureArea = (Rectangle) configuration.getProperty(Configuration.CAPTURE_AREA);

        zoomLbl = new JLabel(captureArea.width + " x " + captureArea.height);

        ImageIcon icon = new ImageIcon(Settings.class.getResource("/zout.jpg"));
        zoomOut = new JButton(icon);
        zoomOut.setPreferredSize(new Dimension(icon.getIconWidth(), icon.getIconHeight()));
        zoomOut.addActionListener(expert);

        icon = new ImageIcon(Settings.class.getResource("/zin.jpg"));
        zoomIn = new JButton(icon);
        zoomIn.setPreferredSize(new Dimension(icon.getIconWidth(), icon.getIconHeight()));
        zoomIn.addActionListener(expert);

        panel.add(zoomOut);
        panel.add(zoomLbl);
        panel.add(zoomIn);

        panel.add(Box.createVerticalStrut(5));

        JLabel label = new JLabel("Mouse Colour");
        colorButton = new JButton();
        colorButton.setPreferredSize(new Dimension(15, 15));
        mouseColor = (Color) configuration.getProperty(Configuration.MOUSE_COLOR);
        colorButton.setBackground(mouseColor);
        colorButton.addActionListener(expert);

        panel.add(label);
        panel.add(colorButton);

        panel.setPreferredSize(new Dimension(200, 100));

        return panel;
    }

    private class ButtonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            Object source = e.getSource();
            if (source == zoomIn || source == zoomOut) {
                Configuration configuration = Configuration.instance();
                Rectangle trgArea = (Rectangle) configuration.getProperty(Configuration.CAPTURE_AREA);
                int width = trgArea.width;
                int height = trgArea.height;
                if (source == zoomIn) {
                    width *= 0.9;
                    height *= 0.9;
                } else if (source == zoomOut) {
                    width *= 1.1;
                    height *= 1.1;
                }

                trgArea = new Rectangle(width, height);
                zoomLbl.setText(trgArea.width + " x " + trgArea.height);
                configuration.setProperty(Configuration.CAPTURE_AREA, trgArea);
            } else if (source == colorButton) {
                mouseColor = JColorChooser.showDialog(Settings.this, "Mouse Colour", Settings.this.mouseColor);
                colorButton.setBackground(mouseColor);
                Configuration.instance().setProperty(Configuration.MOUSE_COLOR, mouseColor);
            }
        }
    }
}
