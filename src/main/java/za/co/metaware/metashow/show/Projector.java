package za.co.metaware.metashow.show;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Sfa on 3/4/2017.
 */
public class Projector extends JFrame {

    private final GraphicsDevice screen;

    private volatile Image projection;

    public Projector(GraphicsDevice screen) {
        super("MetaShow");

        this.screen = screen;

        setUndecorated(true);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
    }

    public void paint(Graphics pg) {
        pg.drawImage(projection, 0, 0, getWidth(), getHeight(), this);
    }

    public void project(Image projection) {
        this.projection = projection;
        repaint();
    }

    public GraphicsDevice getScreen() {
        return screen;
    }
}
