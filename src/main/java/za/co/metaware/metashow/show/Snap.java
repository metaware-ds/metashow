package za.co.metaware.metashow.show;

import za.co.metaware.metashow.exception.TechnicalException;
import za.co.metaware.metashow.util.Configuration;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by Sfa on 3/4/2017.
 */
public class Snap extends Thread {
    private final GraphicsDevice screen;
    private final Projector projector;
    private final long refreshRate;

    private Thread self;
    private boolean running;

    private Robot robot;
    private Rectangle screenArea;
    private Rectangle captureArea;
    private Color mouseColor;

    public Snap(GraphicsDevice screen, Projector projector, long refreshRate) {
        this.projector = projector;
        this.screen = screen;
        this.refreshRate = refreshRate;

        Configuration.instance().addListener(new ConfigListener());
    }

    public void run() {
        running = true;
        self = Thread.currentThread();
        while (running) {
            BufferedImage capture = captureScreen();
            projector.project(capture);
            try {
                Thread.sleep(refreshRate);
            } catch (InterruptedException e) {
            }
        }
    }

    private BufferedImage captureScreen() {
        Point mouseLocation = MouseInfo.getPointerInfo().getLocation();

        int x = mouseLocation.x - captureArea.width / 2;
        int y = mouseLocation.y - captureArea.height / 2;
        int w = captureArea.width;
        int h = captureArea.height;

        int mouseX = w / 2;
        int mouseY = h / 2;

        if (mouseLocation.x < captureArea.width / 2) {
            x = 0;
            mouseX = mouseLocation.x;
        } else if (mouseLocation.x > screenArea.width - captureArea.width / 2) {
            x = screenArea.width - captureArea.width;
            mouseX = captureArea.width - (screenArea.width - mouseLocation.x);
        }
        if (mouseLocation.y < captureArea.height / 2) {
            y = 0;
            mouseY = mouseLocation.y;
        } else if (mouseLocation.y > screenArea.height - captureArea.height / 2) {
            y = screenArea.height - captureArea.height;
            mouseY = captureArea.height - (screenArea.height - mouseLocation.y);
        }
        Rectangle mouseArea = new Rectangle(x, y, w, h);
        BufferedImage capture = robot.createScreenCapture(mouseArea);
        Graphics g = capture.getGraphics();
        g.setColor(mouseColor);
        g.fillOval(mouseX - 2, mouseY - 2, 4, 4);
        return capture;
    }

    public void config() {
        if (robot == null) {
            try {
                robot = new Robot();
            } catch (AWTException e) {
                throw new TechnicalException("Could not create source screen capture device");
            }
        }
        Configuration configuration = Configuration.instance();
        screenArea = screen.getDefaultConfiguration().getBounds();
        captureArea = (Rectangle) configuration.getProperty(Configuration.CAPTURE_AREA);
        if (captureArea == null) {
            captureArea = projector.getScreen().getDefaultConfiguration().getBounds();
            configuration.setProperty(Configuration.CAPTURE_AREA, captureArea);
        }
        mouseColor = (Color) configuration.getProperty(Configuration.MOUSE_COLOR);
    }

    public void stopRunning() {
        running = false;
        self.interrupt();
    }

    private class ConfigListener implements Configuration.UpdateListener {

        @Override
        public void update(String key, Object oldValue, Object newValue) {
            switch (key) {
                case Configuration.CAPTURE_AREA:
                case Configuration.MOUSE_COLOR:
                    config();
                    break;
            }
        }
    }
}
